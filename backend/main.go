package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"

	"github.com/akrylysov/pogreb"
)

func main() {
	db, err := pogreb.Open("db.pogreb", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer db.Close()

	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)

	http.HandleFunc("/api/v1/url", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.Error(w, "Method is not supported.", http.StatusNotFound)
			return
		}
		urlParaEncurtar, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		host := r.Host
		id := randSeq(7)

		db.Put([]byte(id), urlParaEncurtar)

		fmt.Fprintf(w, "%s/r/%s", host, id)
	})

	http.HandleFunc("/r/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			http.Error(w, "Method is not supported.", http.StatusNotFound)
			return
		}

		path := r.URL.Path
		if len(path) < 7 {
			http.NotFound(w, r)
			return
		}
		id := path[len(path)-7 : len(path)]

		val, err := db.Get([]byte(id))
		if err != nil {
			http.NotFound(w, r)
			return
		}

		http.Redirect(w, r, string(val), http.StatusFound)
	})

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
